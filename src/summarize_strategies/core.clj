(comment summarize-strategies.core
  (:gen-class))

(ns summarize-strategies.core

  (:require
   [summarize-strategies.utils :as utils]


   [clojure.java.io :as io]
   [clojure.string :as str]
   [clj-http.client :as client]
   [clj-time.format :as tf]
   [clj-time.core :as tt]
   [incanter.interpolation :refer :all]
   [incanter [core :refer [$]
              :as incanter$]
    [core :as incanter]
    [stats :as stats]
    [io :as io2]
    [charts :as charts]
    [datasets :as dataset]

                                        ;[interpolation :as interpolation]

    ]
   [clojure.tools.cli :refer [cli]]


   ;[matrix.utils :as u]
   )

  (:use clojure.pprint)
  (:require [clojure.tools.cli :refer [parse-opts]])

  (:gen-class))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]


  (def cmd-args (let [{:keys [options arguments summary errors]} (parse-opts args
                                                                           [


                                                                            ["-f" "--file" "file"
                                                                             :default nil
                                        ;:parse-fn #(str/split % #",")

                                                                             ]





                                                                            ["-h" "--help" "Print this help" :default false]

                                                                            ])]
                                        ;(println summary)
                                        ;(println (:tickers options))
                                        ;(println options )
                arguments
                                        ;(println summary )
                                        ;(println errors )







                ))


;(def cmd-args [20171205 "CGNX,GWPH,PBYI,TTWO,SHOP" "fatwa"])
(def file- (first cmd-args))






 (def lines (str/split (slurp file-) #"\n"))

(def howmany (count (first (map #(drop 1 (str/split % #" ")) lines))))

(defn nth-sharpe [n]
  (let [ data (map #(drop 1 (str/split % #" ")) lines)
        s (map read-string (map #(nth % n) data))
        m (stats/mean s)
        std (stats/sd s)
        sharpe (cond (zero? std) 0 :else (/ m std))
        nonzero-s (filter #(not (zero? %)) s )

        out [ (utils/round m) (utils/round sharpe) (count s) (count nonzero-s) (apply min s) (apply max s)  ]
ks (map keyword ["mean" "sharpe" "count" "nonzero-count" "min" "max"])

        ]
      (zipmap ks out))
  )
(def result (map nth-sharpe (range howmany)))

(pprint (reverse (sort-by #(:sharpe (second %)) (map vector (range howmany) result))))






  )


;(def file- (str utils/HOME "/Dropbox/data/results-zippers.data"))

